<?php
class M_common extends CI_Model
{

    public function gen_menu($parent = 0, $user_id = 0)
    {
        $s = "SELECT
                    m.*
                from
                    (
                    select
                        *
                    from
                        ms_user
                    where
                        user_id = $user_id) ug
                join ms_group_menu mgm on
                    mgm.group_id = ug.group_id
                join ms_menu m on
                    m.menu_id = mgm.menu_id 
                where
                    m.menu_aktif = true
                    and m.menu_parent_id = $parent
                order by
                    m.menu_kode ";
        $ret = "";
        $d  = $this->db->query($s);

        foreach ($d->result() as $key => $value) {
            // cek ada anak
            $sa = "SELECT
                    m.*
                from
                    (
                    select
                        *
                    from
                        ms_user
                    where
                        user_id = $user_id) ug
                join ms_group_menu mgm on
                    mgm.group_id = ug.group_id
                join ms_menu m on
                    m.menu_id = mgm.menu_id 
                where
                    m.menu_aktif = true
                    and m.menu_parent_id = $value->menu_id";
            $da  = $this->db->query($sa);
            if ($da->num_rows() == 0 && $parent == 0) { // tidak ada anak
                $url = !empty($value->menu_link) ? $value->menu_link : '#';
                $ret .= "<li class='nav-item'>
                            <a class='nav-link' href='" . base_url($url) . "'>
                                <i class='$value->menu_icon'></i>
                                <span>$value->menu_name</span>
                            </a>
                        </li>";
                // $ret .= ' <li class="kt-menu__item " aria-haspopup="true">';
                // $ret .= '<a href="' . base_url($value->menu_link) . '" class="kt-menu__link "><i class="kt-menu__link-icon ' . $value->menu_icon . '"></i><span class="kt-menu__link-text">' . $value->menu_name . '</span></a>';
                // $ret .= '</li>';
            } elseif ($da->num_rows() > 0 && $parent == 0) { // ada anak tapi parent ke 0
                $ret .= '<li class="nav-item">
                            <a class="nav-link collapsed" href="javascript:;" data-toggle="collapse" data-target="#collapse' . $value->menu_id . '" aria-expanded="true" aria-controls="collapse' . $value->menu_id . '">
                                <i class="' . $value->menu_icon . '"></i>
                                <span>' . $value->menu_name . '</span>
                            </a>
                            <div id="collapse' . $value->menu_id . '" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                                <div class="bg-white py-2 collapse-inner rounded">';
                $ret .= $this->gen_menu($value->menu_id, $user_id);
                $ret .= '
                                </div>
                            </div>
                        </li>';
                // $ret .= '<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">';
                // $ret .= '<a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon ' . $value->menu_icon . '"></i><span class="kt-menu__link-text">' . $value->menu_name . '</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>';
                // $ret .= $this->gen_menu($value->menu_id, $user_id);
                // $ret .= '</li>';
                // $ret .= '<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover">';
                // $ret .= '<a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-icon ' . $value->menu_icon . '"></i><span class="kt-menu__link-text">' . $value->menu_name . '</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>';
                // $ret .= $this->gen_menu($value->menu_id, $user_id);
                // $ret .= '</li>';
            } elseif ($da->num_rows() == 0 && $parent > 0) { // tidak ada anak,  tapi bukan parent ke 0
                $url = !empty($value->menu_link) ? $value->menu_link : '#';
                $ret .=  '<a class="collapse-item" href="' . base_url($url) . '">' . $value->menu_name . '</a>';
                // $ret .=  '<li class="kt-menu__item " aria-haspopup="true"><a href="' . base_url($value->menu_link) . '" class="kt-menu__link "><i class="kt-menu__link-bullet kt-menu__link-bullet--dot"><span></span></i><span class="kt-menu__link-text">' . $value->menu_name . '</span></a></li>';
            } else { // ada anak dan bukan parent ke 0
                $ret .= '<div>
                            <a class="collapse-item sub-nav-link collapsed" href="javascript:;" data-toggle="collapse" data-target="#collapse' . $value->menu_id . '" aria-expanded="true" aria-controls="collapse' . $value->menu_id . '">
                                <i class="' . $value->menu_icon . '"></i>
                                <span>' . $value->menu_name . '</span>
                            </a>
                            <div id="collapse' . $value->menu_id . '" class="collapse" aria-labelledby="headingTwo" data-parent="#collapse' . $value->menu_parent_id . '">
                                <div class="bg-white py-2">';
                $ret .= $this->gen_menu($value->menu_id, $user_id);
                $ret .= '
                                </div>
                            </div>
                        </div>';
                // $ret .= '<li class="kt-menu__item  kt-menu__item--submenu" aria-haspopup="true" data-ktmenu-submenu-toggle="hover"><a href="javascript:;" class="kt-menu__link kt-menu__toggle"><i class="kt-menu__link-bullet kt-menu__link-bullet--line"><span></span></i><span class="kt-menu__link-text">' . $value->menu_name . '</span><i class="kt-menu__ver-arrow la la-angle-right"></i></a>';
                // $ret .= $this->gen_menu($value->menu_id, $user_id);
                // $ret .= '</li>';
            }
        }
        return $ret;
    }
}
