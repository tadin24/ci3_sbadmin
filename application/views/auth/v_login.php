<!DOCTYPE html>
<html>

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?= $judul ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="<?= base_url('') ?>node_modules/startbootstrap-sb-admin-2/css/sb-admin-2.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?= base_url() ?>node_modules/@fortawesome/fontawesome-free/css/all.css">
  <!-- Theme style -->
  <script src="<?= base_url('') ?>node_modules/jquery/dist/jquery.js"></script>
  <script src="<?= base_url('') ?>node_modules/bootstrap/dist/js/bootstrap.js"></script>


  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link rel="stylesheet" href="<?= base_url() ?>assets/css/custom-login.css">
</head>

<body>
  <div class="wrapper fadeInDown">
    <div id="formContent">
      <!-- Tabs Titles -->

      <!-- Icon -->
      <div class="fadeIn first">
        <img src="<?= base_url() ?>assets/images/component/user.png" id="icon" alt="User Icon" style="max-width: 40%;" />
      </div>

      <!-- Login Form -->
      <form method="POST" action="">
        <?= $this->session->flashdata('pesan'); ?>
        <input type="text" id="username" class="fadeIn second" name="username" placeholder="username">
        <input type="password" id="password" class="fadeIn third" name="password" placeholder="password">
        <input type="submit" class="fadeIn fourth" value="Log In" id="btnLogin" name="login">
      </form>

    </div>
  </div>
</body>

</html>