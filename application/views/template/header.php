<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title><?= $title ?></title>

  <link rel="stylesheet" href="<?php echo base_url('assets/leaflet/leaflet.css') ?>">

  <!-- Custom fonts for this template-->
  <link href="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/css/sb-admin-2.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">
  <link href="<?= base_url() ?>node_modules/select2/dist/css/select2.css" rel="stylesheet">
  <link href="<?= base_url() ?>node_modules/jstree/dist/themes/default/style.css" rel="stylesheet">
  <link href="<?= base_url() ?>node_modules/jquery-ui-dist/jquery-ui.css" rel="stylesheet">
  <link href="<?= base_url() ?>node_modules/sweetalert2/dist/sweetalert2.css" rel="stylesheet">

  <style type="text/css">
    .sub-nav-link[data-toggle=collapse]::after {
      content: '\f107';
      font-family: 'Font Awesome 5 Free';
      width: 1rem;
      text-align: center;
      float: right;
      font-weight: 900;
    }

    .sub-nav-link[data-toggle=collapse].collapsed::after {
      font-family: 'Font Awesome 5 Free';
      width: 1rem;
      text-align: center;
      float: right;
      font-weight: 900;
      content: '\f105';
    }

    .error {
      color: #e74a3b;
      border-color: #e74a3b;
      font-size: 1rem;
      width: 100%;
    }

    .form-unit .select2-container,
    .form-unit .select2-selection {
      width: 100% !important;
    }

    .select2 {
      width: 100% !important;
    }

    /* .select2-selection__rendered {
    line-height: 1em !important;
  } */

    .select2-container .select2-selection--single {
      height: 35px !important;
    }

    .select2-selection__arrow {
      height: 34px !important;
    }
  </style>


  <!-- Bootstrap core JavaScript-->
  <script src="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/vendor/jquery/jquery.min.js"></script>
  <script src="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="<?= base_url() ?>node_modules/startbootstrap-sb-admin-2/vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="<?= base_url() ?>node_modules/jquery-validation/dist/jquery.validate.js"></script>
  <script type="text/javascript" src="<?= base_url() ?>node_modules/jquery-validation/dist/localization/messages_id.js"> </script>
  <script src="<?= base_url() ?>node_modules/jquery-validation/dist/additional-methods.js"></script>
  <script src="<?= base_url() ?>node_modules/datatables.net/js/jquery.dataTables.js"></script>
  <script src="<?= base_url() ?>node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js"></script>
  <script src="<?= base_url() ?>node_modules/select2/dist/js/select2.full.js"></script>
  <script src="<?= base_url() ?>node_modules/jstree/dist/jstree.js"></script>
  <script src="<?= base_url() ?>node_modules/jquery-ui-dist/jquery-ui.js"></script>
  <script src="<?= base_url() ?>node_modules/chart.js/dist/Chart.js"></script>
  <script src="<?= base_url() ?>node_modules/sweetalert2/dist/sweetalert2.js"></script>

  <script>
    $.validator.addMethod('filesize', function(value, element, param) {
      return this.optional(element) || (element.files[0].size <= param)
    }, 'Ukuran maksimum file tidak boleh lebih dari {0} kb');
  </script>
</head>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">