<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('m_dashboard');

        $this->load->helper('url');
    }

    public function index()
    {
        $data = array(
            'title' => 'Data dashboard',
        );
        $this->my_theme('v_dashboard', $data);
    }
}
