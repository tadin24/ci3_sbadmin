-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 21, 2021 at 04:00 PM
-- Server version: 10.4.19-MariaDB
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ci3_sbadmin`
--

-- --------------------------------------------------------

--
-- Table structure for table `ms_group`
--

CREATE TABLE `ms_group` (
  `group_id` int(10) UNSIGNED NOT NULL,
  `group_name` varchar(255) DEFAULT NULL,
  `group_aktif` int(2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ms_group`
--

INSERT INTO `ms_group` (`group_id`, `group_name`, `group_aktif`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'Admin', 1, '2021-04-02 22:07:08', 1, '2021-04-18 00:00:00', 1),
(2, 'User', 1, '2021-04-02 22:07:08', 1, '2021-04-03 00:10:05', 2);

-- --------------------------------------------------------

--
-- Table structure for table `ms_group_menu`
--

CREATE TABLE `ms_group_menu` (
  `group_id` int(10) UNSIGNED DEFAULT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ms_group_menu`
--

INSERT INTO `ms_group_menu` (`group_id`, `menu_id`, `created_at`, `created_by`) VALUES
(1, 1, '2021-04-02 22:07:09', 1),
(1, 2, '2021-04-02 22:07:09', 1),
(1, 3, '2021-04-02 22:07:09', 1),
(1, 4, '2021-04-02 22:07:09', 1),
(3, 1, '2021-06-21 14:57:45', NULL),
(3, 2, '2021-06-21 14:57:49', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `ms_menu`
--

CREATE TABLE `ms_menu` (
  `menu_id` int(10) UNSIGNED NOT NULL,
  `menu_kode` varchar(255) NOT NULL,
  `menu_name` varchar(255) NOT NULL,
  `menu_link` varchar(255) DEFAULT NULL,
  `menu_icon` varchar(255) DEFAULT NULL,
  `menu_aktif` int(2) DEFAULT NULL,
  `menu_level` int(11) DEFAULT NULL,
  `menu_parent_id` int(11) DEFAULT NULL,
  `menu_parent_kode` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ms_menu`
--

INSERT INTO `ms_menu` (`menu_id`, `menu_kode`, `menu_name`, `menu_link`, `menu_icon`, `menu_aktif`, `menu_level`, `menu_parent_id`, `menu_parent_kode`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, '01', 'Admin System', '#', 'fa fa-user-cog', 1, 1, 0, '', '2021-04-02 22:07:08', 1, '2021-04-10 00:00:00', 1),
(2, '01.1', 'Master User', 'ms_user', '', 1, 2, 1, '01', '2021-04-02 22:07:08', 1, '2021-04-02 22:07:08', 1),
(3, '01.2', 'Master Group', 'ms_group', '', 1, 2, 1, '01', '2021-04-02 22:07:08', 1, '2021-04-02 22:07:08', 1),
(4, '01.3', 'Master Menu', 'ms_menu', '', 1, 2, 1, '01', '2021-04-02 22:07:08', 1, '2021-04-02 22:07:08', 1);

-- --------------------------------------------------------

--
-- Table structure for table `ms_user`
--

CREATE TABLE `ms_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) NOT NULL,
  `user_fullname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `group_id` int(11) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `user_aktif` int(2) DEFAULT NULL,
  `is_superuser` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ms_user`
--

INSERT INTO `ms_user` (`user_id`, `username`, `user_fullname`, `email`, `group_id`, `password`, `user_aktif`, `is_superuser`, `created_at`, `created_by`, `updated_at`, `updated_by`) VALUES
(1, 'admin', 'Administrator', NULL, 1, '21232f297a57a5a743894a0e4a801fc3', 1, 0, NULL, NULL, '2021-06-21 00:00:00', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `ms_group`
--
ALTER TABLE `ms_group`
  ADD PRIMARY KEY (`group_id`);

--
-- Indexes for table `ms_menu`
--
ALTER TABLE `ms_menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Indexes for table `ms_user`
--
ALTER TABLE `ms_user`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `ms_group`
--
ALTER TABLE `ms_group`
  MODIFY `group_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ms_menu`
--
ALTER TABLE `ms_menu`
  MODIFY `menu_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `ms_user`
--
ALTER TABLE `ms_user`
  MODIFY `user_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
